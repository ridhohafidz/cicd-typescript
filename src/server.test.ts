import supertest from 'supertest'

import app from './server'
const request = supertest(app)


it('Call the /youtube endpoint', async done => {
    const res = await request.get('/youtube')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Hello, youtube indonesia!')
    done()
})
it('Call the / endpoint', async done => {
    const res = await request.get('/')
    expect(res.status).toBe(200)
    expect(res.text).toBe('This App is running properly!')
    done()
})
it('Call the /pong endpoint', async done => {
    const res = await request.get('/ping')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Pong!')
    done()
})
it('Call the /hello/:name endpoint', async done => {
    const res = await request.get('/hello/Ridho')
    expect(res.status).toBe(200)
    expect(res.body.message).toBe('Hello Ridho')
    done()
})
it('Call the /ridho endpoint', async done => {
    const res = await request.get('/ridho')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Ini buatan ridho!')
    done()
})
it('Call the /biznetgio endpoint', async done => {
    const res = await request.get('/biznetgio')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Halo Biznet Gio Indonesia!')
    done()
})

  
